var del = require('del');
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var data = require('./src/data/data.json')
var imagemin = require('gulp-imagemin');
var webserver = require('gulp-webserver');
var gulpSequence = require('gulp-sequence')
var sourcemaps = require('gulp-sourcemaps');
var handlebars = require('gulp-compile-handlebars');

var paths = {
  scripts: './src/scripts/**/*.js',
  styles: './src/styles/**/*.scss',
  maincss: './src/styles/main.scss',
  templates: './src/templates/*.hbs',
  templatesAll: './src/templates/**/*.hbs',
  images: './src/imgs/**/*',
  fonts: './src/fonts/**/*'
}

gulp.task('clean', function() {
  return del(['./dist/']);
});

gulp.task('templates', function () {
    var templateData = data,
    options = {
        ignorePartials: true,
        batch : ['./src/templates/partials/'],
    }
 
    return gulp.src('./src/templates/*.hbs')
        .pipe(handlebars(templateData, options))
        .pipe(rename(function(path) {
            path.extname = '.html';
        }))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('scripts', function() {
  return gulp.src(paths.scripts)
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('scripts.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/public/js/'));
});

gulp.task('styles', function() {
    gulp.src(paths.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/public/css/'));
});

gulp.task('images', function() {
  return gulp.src(paths.images)
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest('./dist/public/imgs/'));
});

gulp.task('fonts', function() {
  return gulp.src(paths.fonts)
    .pipe(gulp.dest('./dist/public/fonts/'));
});

gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.styles, ['styles']);
  gulp.watch(paths.templatesAll, ['templates']);
  gulp.watch(paths.images, ['images']);
});

gulp.task('webserver', function() {
  gulp.src('./')
    .pipe(webserver({
      livereload: true,
      directoryListing: true,
      open: true,
      host: '0.0.0.0',
      port: '3000'
    }));
});

gulp.task('default', gulpSequence('clean', [ 'scripts', 'images', 'fonts', 'styles'],  'templates', 'webserver', 'watch'));
gulp.task('sandbox', gulpSequence('clean', [ 'scripts', 'images', 'fonts', 'styles'],  'templates', 'watch'));


