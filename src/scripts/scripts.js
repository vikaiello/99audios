$(document).ready(function() {
  var faq = $('.faq');
  var clientsCarousel = $('.clients-carousel');
  var showcaseSpeakers = $('.speakers-showcase');
  var testimonials = $('.testimonials');
  if (faq) toggleFAQ();
  if (clientsCarousel) callClientsCarousel();
  if (testimonials) callTestimonialsCarousel();
  if (showcaseSpeakers) filterSpeakers();

});

function toggleFAQ() {
  var buttons = $('.faq__item-button');
  var faqItens = $('.faq__item');

  buttons.on('click', function() {
    var parent = $(this).parent();
    if (!parent.hasClass('faq__item--active')) {
      faqItens.removeClass('faq__item--active');
      parent.addClass('faq__item--active');
    } else {
      faqItens.removeClass('faq__item--active');
    }
  });
}

function callClientsCarousel() {
  var carousel = $('.clients-carousel__carousel');
  carousel.slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          arrows: false,
          dots: true
        }
      }
    ]
  });   
}

function callTestimonialsCarousel() {
  var carousel = $('.testimonials__list');
  carousel.slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          arrows: false,
          dots: true
        }
      }
    ]
  });   
}

function filterSpeakers() {
  var filterButtons = $('[data-filter]');
  var itens = $('.speakers-showcase__item');
  var buttonActiveClass = 'speakers-showcase__filter-button--active';
  var itemHideClass = 'speakers-showcase__item--hide';

  filterButtons.on('click', function() {
    var button = $(this);
    if (button.hasClass(buttonActiveClass)) {
      filterButtons.removeClass(buttonActiveClass);
      itens.removeClass(itemHideClass);
    } else {
      var selectedFilter = button.attr('data-filter');
      var selectedItens = $('[data-type="' + selectedFilter + '"]');
      itens.addClass(itemHideClass);
      selectedItens.removeClass(itemHideClass);
      filterButtons.removeClass(buttonActiveClass);
      button.addClass(buttonActiveClass);
    }

  });
}
